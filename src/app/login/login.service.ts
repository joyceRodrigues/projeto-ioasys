import { Injectable, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import {Headers} from '@angular/http'
import { CookieService } from 'ngx-cookie-service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';

@Injectable()
export class LoginService {

constructor(private httpClient : HttpClient, private cookieService: CookieService, private router: Router) { }

      private SERVIDOR_LOGIN = "http://54.94.179.135:8090/api/v1/users/auth/sign_in";

      private objHeader;

      autenticar(email, senha) {

        this.httpClient.post(this.SERVIDOR_LOGIN,
          {
            email : email,
            password : senha
          }, {observe: 'response'})
          .subscribe(
            resposta => {
              this.objHeader = resposta.headers;
              this.gravarToken(this.objHeader);
              this.router.navigate(['/empresas-lista']);
            }, erro => {
              this.cookieService.set( "access-token","");
            }
    )
}

gravarToken(obj:any){
      this.cookieService.set( "access-token", this.objHeader.get("access-token"));
      this.cookieService.set( "client", this.objHeader.get("client"));
      this.cookieService.set( "uid", this.objHeader.get("uid"));
    }
}