import {Routes} from '@angular/router';
import { LoginComponent } from './login/login.component';
import { EmpresaListaComponent } from './empresas/empresa-lista/empresa-lista.component';
import { EmpresaDescricaoComponent } from './empresas/empresa-descricao/empresa-descricao.component';
import { EmpresasComponent } from './empresas/empresas.component';

export const ROUTES: Routes = [
    {
        path:'',
        component: LoginComponent
    },
    {
        path:'login',
        component: LoginComponent
    },
    {
        path:'empresas',
        component: EmpresasComponent
    },
    {
        path:'empresas/:id',
        component: EmpresaDescricaoComponent
    }

]