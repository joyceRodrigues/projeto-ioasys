import { Component, OnInit, Input } from '@angular/core';
import { EmpresasService } from './empresa-lista.service';
import { Enterprises } from '../empresa-descricao/empresa-descricao.model';
import {EnterpriseUn} from '../empresa-descricao/empresa-descricao.model';
import {Enterprise} from '../empresa-descricao/empresa-descricao.model'


@Component({
  selector: 'app-empresa-lista',
  templateUrl: './empresa-lista.component.html',
  styleUrls: ['./empresa-lista.component.css']
})
export class EmpresaListaComponent implements OnInit {

  private ahEmpresaDetalhe: boolean;


  buscaAberta: boolean = false;
  objeto: Enterprises;
  empresas:Enterprise[] = [];
  empresa: EnterpriseUn;

  vazio: boolean = true;

  @Input() listagem
  constructor(private empresasService: EmpresasService){}

      listaTodasEmpresas(){
        this.empresasService.getEmpresas().subscribe(data => {this.objeto = data; this.empresas = this.objeto.enterprises; this.vazio = false;});
      }
  
      buscaEmpresaPorId(id:string){
        this.empresasService.getEmpresasPorId(id).subscribe(data => {this.empresa = data; console.log(this.empresa.enterprise.id)});
      }
  
      filtrarEmpresasTipoNome(parametro: string){
        this.empresasService.buscarEmpresas(parametro).subscribe(data => { this.objeto = data; this.empresas = this.objeto.enterprises; this.vazio = false;});
      }
      abrir() {
        if(!this.buscaAberta){
          document.getElementById("busca").style.width = "300px";
          document.getElementById("lupa").style.borderBottom = "1px solid #fff";
          if(window.innerWidth < 520){
            document.getElementById("logo").style.display = "none";
          }
          this.buscaAberta = true;
        }else{
          
          (document.getElementById("input-busca") as HTMLInputElement).value = "";
          document.getElementById("busca").style.width = "0px";
          document.getElementById("lupa").style.borderBottom = "none";
          document.getElementById("logo").style.display = "initial";
          this.buscaAberta = false;
        }
      }
      ngOnInit(){
        this.ahEmpresaDetalhe = false
  }
}