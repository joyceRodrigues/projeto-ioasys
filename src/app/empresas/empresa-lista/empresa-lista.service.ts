import { Injectable } from '@angular/core'
import { HttpClient, HttpHeaders } from '@angular/common/http'
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs/Observable';
import { Enterprises } from '../empresa-descricao/empresa-descricao.model'
import { EnterpriseUn } from '../empresa-descricao/empresa-descricao.model'



@Injectable()
export class EmpresasService{

constructor(private httpClient:HttpClient, private cookieService: CookieService){};

  getEmpresas():Observable<Enterprises>{

    let headers = new HttpHeaders();
    headers = headers.set("access-token", this.cookieService.get("access-token"))
    .set("client",this.cookieService.get("client"))
    .set("uid",this.cookieService.get("uid"));

    return this.httpClient.get<Enterprises>('http://54.94.179.135:8090/api/v1/enterprises',{headers: headers})
  }


  getEmpresasPorId(id: string):Observable<EnterpriseUn>{

    let headers = new HttpHeaders();
    headers = headers.set("access-token", this.cookieService.get("access-token"))
    .set("client",this.cookieService.get("client"))
    .set("uid",this.cookieService.get("uid"));
    
    return this.httpClient.get<EnterpriseUn>(`http://54.94.179.135:8090/api/v1/enterprises/${id}`,{headers: headers})
  }


  buscarEmpresas(parametro: string):Observable<Enterprises>{

    let numero:number = parseInt(parametro);

    let headers = new HttpHeaders();
    headers = headers.set("access-token", this.cookieService.get("access-token"))
    .set("client",this.cookieService.get("client"))
    .set("uid",this.cookieService.get("uid"));

    if(!isNaN(numero)){
      return this.httpClient.get<Enterprises>(`http://54.94.179.135:8090/api/v1/enterprises?enterprise_types=${numero}`,{headers: headers})
    }else{
      return this.httpClient.get<Enterprises>(`http://54.94.179.135:8090/api/v1/enterprises?name=${parametro}`,{headers: headers})
    }
  }
}