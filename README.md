# README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto Empresas.

### Tecnologias Utilizadas ###

* Angular-cli

### ESCOPO DO PROJETO ###

* Login e acesso de Usuário já registrado
* Listagem de Empresas
* Detalhamento de Empresas
* Filtro de Empresas por nome e tipo


### Considerações ###
 * Um tanto quanto desafiador esse foi meu primeiro projeto usando angular do 0, foi muito produtivo de fazer e particularmente me trouxe mais conhecimento do que eu imaginava.
 * Agradeço por ter tantado ao máximo e ter tirado um bom tempo para a realização deste.
 * Infelizmente por morar dentro da faculdade tive dificuldades em autorização, tanto para dar push quanto para as rotas do postman.
 * No entanto não conseguir testar tudo que queria, vantagens e desvantagens de ter um WIFI poderoso da faculdade (risos de nervoso)
